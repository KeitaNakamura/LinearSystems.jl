__precompile__()

module LinearSystems

using MacroTools
using Base.@pure

export LinearSystem, linsolve
export @linsystem, @linsolve, @equation

include("assembly.jl")
include("variables.jl")
include("term.jl")
include("equation.jl")
include("system.jl")
include("macros.jl")

end
