struct Assembly{T <: Real}
    A::Matrix{T}
    b::Vector{T}
end

@inline function Assembly{T}(dim::Int) where T
    Assembly(zeros(T, dim, dim), zeros(T, dim))
end

@inline ndofs(assembly::Assembly) = length(assembly.b)
@inline solve(assembly::Assembly) = assembly.A \ assembly.b
