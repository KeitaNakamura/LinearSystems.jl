struct Equation{Terms <: Tuple, T <: Real, Rhs <: Union{T, AbstractVector{T}}}
    terms::Terms
    rhs::Rhs
end

@inline function Equation(rhs::Rhs, terms::Term...) where Rhs
    Equation{typeof(terms), eltype(rhs), Rhs}(terms, rhs)
end

@pure function valuetype(::Type{<: Equation{Terms}}) where Terms
    return promote_type((valuetype(term) for term in Terms.parameters)...)
end

@generated function assemble!(assembly::Assembly, vs::VariableSet, equation::Equation{Terms}, I::AbstractVector{Int}) where {Terms}
    exps = map(1:length(Terms.parameters)) do i
        return quote
            @inbounds begin
                J = dofindex(equation.terms[$i], vs) + offset(equation.terms[$i], vs)
                assemble!(assembly.A, equation.terms[$i], I, J)
            end
        end
    end
    ex = Expr(:let, Expr(:block, exps...))
    return quote
        $ex
        assembly.b[I] = equation.rhs
        return assembly
    end
end

@inline function ndofs(equation::Equation, vs::VariableSet)
    # use first term to determine dimension of equation
    return ndofs(equation.terms[1], vs)
end
@inline function dofindex(equation::Equation, vs::VariableSet)
    return 1:ndofs(equation, vs)
end
