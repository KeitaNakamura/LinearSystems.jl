struct LinearSystem{Eqs <: Tuple, N, T}
    equations::Eqs
    vs::VariableSet{N, T}
end

@inline ndofs(system::LinearSystem) = ndofs(system.vs)

@pure function valuetype(system::Type{<: LinearSystem{Eqs}}) where {Eqs}
    return promote_type((valuetype(eq) for eq in Eqs.parameters)...)
end

function linsolve(system::T) where {T <: LinearSystem}
    assembly = Assembly{valuetype(T)}(ndofs(system))
    assemble!(assembly, system)
    x = solve(assembly)
    extract_results(x, system.vs)
end

@generated function assemble!(assembly::Assembly, system::LinearSystem{Eqs}) where {Eqs}
    exps = map(1:length(Eqs.parameters)) do i
        return quote
            @inbounds begin
                I = dofindex(system.equations[$i], vs) + offset
                assemble!(assembly, vs, system.equations[$i], I)
                offset += length(I)
            end
        end
    end
    ex = Expr(:let, Expr(:block, exps...))
    return quote
        vs = system.vs

        offset = 0
        $ex

        nvar = ndofs(assembly)
        if nvar != offset
            error("The number of equations ($offset) does not match the number of variables ($nvar).")
        end

        return assembly
    end
end
