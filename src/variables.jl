abstract type AbstractVariable{name} end

@inline variablename(::Type{<: AbstractVariable{name}}) where {name} = name

struct ScalarVariable{name} <: AbstractVariable{name}
end

@inline ndofs(x::ScalarVariable) = 1
@inline dofindex(x::ScalarVariable, offset::Int) = 1 + offset

struct VectorVariable{name} <: AbstractVariable{name}
    dim::Int
end

@inline ndofs(x::VectorVariable) = x.dim
@inline dofindex(x::VectorVariable, offset::Int) = (1:x.dim) + offset


struct VariableSet{N, T <: NTuple{N, AbstractVariable}}
    vars::T
    offsets::NTuple{N, Int}
end

@generated function VariableSet(vars::AbstractVariable...)
    offsets = Any[0]
    for i in 1:length(vars)-1
        push!(offsets, :($(offsets[i]) + ndofs(vars[$i])))
    end
    ex = Expr(:tuple, offsets...)
    return quote
        $(Expr(:meta, :inline))
        @inbounds return VariableSet(vars, $ex)
    end
end

@generated function offset(vs::VariableSet{N, T}, ::Type{Val{name}}) where {N, T, name}
    names = map(variablename, T.parameters)
    i = findfirst(names, name)
    i == 0 && throw(ArgumentError("no name matching $name"))
    return quote
        $(Expr(:meta, :inline))
        @inbounds return vs.offsets[$i]
    end
end

@generated function ndofs(vs::VariableSet{N, T}, ::Type{Val{name}}) where {N, T, name}
    names = map(variablename, T.parameters)
    i = findfirst(names, name)
    i == 0 && throw(ArgumentError("no name matching $name"))
    return quote
        $(Expr(:meta, :inline))
        @inbounds return ndofs(vs.vars[$i])
    end
end

@generated function ndofs(vs::VariableSet{N}) where N
    ex = Expr(:call, :+, [:(ndofs(vs.vars[$i])) for i in 1:N]...)
    return quote
        $(Expr(:meta, :inline))
        @inbounds return $ex
    end
end

@generated function extract_results(x::Vector, vs::VariableSet{N}) where N
    ex = Expr(:tuple, [:(x[dofindex(vs.vars[$i], vs.offsets[$i])]) for i in 1:N]...)
    return quote
        $(Expr(:meta, :inline))
        @inbounds return $ex
    end
end
