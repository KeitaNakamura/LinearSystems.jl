struct Term{name, T <: Real, Coef <: Union{T, AbstractVecOrMat{T}}, Index <: Union{Int, AbstractVector{Int}, Colon}}
    coef::Coef
    index::Index
end

@inline function Term{name}(coef::Coef, index::Index) where {name, Coef, Index}
    Term{name, eltype(coef), Coef, Index}(coef, index)
end

@pure valuetype(::Type{<: Term{name, T}}) where {name, T} = T

@inline offset(term::Term{name}, vs::VariableSet) where {name} = offset(vs, Val{name})

@inline dofindex(term::Term{name, T, Coef}, vs::VariableSet) where {name, T, Coef} = term.index
@inline dofindex(term::Term{name, T, Coef, Colon}, vs::VariableSet) where {name, T, Coef} = 1:ndofs(vs, Val{name})

# use index of Term when coef is scalar
@inline ndofs(term::Term{name, T, <: Real}, vs::VariableSet) where {name, T} = length(dofindex(term, vs))

# use size of coef when coef is vector or matrix
@inline ndofs(term::Term{name, T, <: AbstractVecOrMat}, vs::VariableSet) where {name, T} = size(term.coef, 1)

@inline function assemble!(A::AbstractVecOrMat, term::Term{name, T, <: AbstractVecOrMat}, I, J) where {name, T}
    #=
    axpy!(a, X, Y)
    Overwrite Y with a*X + Y. Returns Y.
    =#
    BLAS.axpy!(one(T), term.coef, @view A[I,J])
end

# This works well when index of term is empy vector.
function assemble!(A::AbstractVecOrMat, term::Term{name, T, <: Real}, I, J) where {name, T}
    @assert length(I) == length(J)
    checkbounds(A, I, J)
    @inbounds for k in 1:length(I)
        A[I[k], J[k]] += term.coef
    end
end
