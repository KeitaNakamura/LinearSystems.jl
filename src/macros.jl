struct SymbolParameter{T} end
SymbolParameter(s::Symbol) = SymbolParameter{s}()

function termexpr(name::Symbol, ex)
    index = indexexpr(name, ex)
    coef = coefexpr(name, ex)
    return :($(Term{name})($coef, $index))
end

################################################################################
#
# @equation(names..., expr)
#   construct Equation
#
################################################################################

macro equation(args...)
    names = collect(args[1:end-1])
    eqs = extract_equations(args[end])
    exps = []
    for eq in eqs
        append!(exps, eqexpr(names, eq))
    end
    if length(exps) == 1
        return esc(exps[1])
    else
        return esc(Expr(:tuple, exps...))
    end
end

function extract_equations(ex::Expr)
    return isexpr(ex, :block) ? rmlines(ex).args : [ex]
end

function eqexpr(names::Vector{Symbol}, ex::Expr)
    if @capture(ex, lhs_ == rhs_)
        return [eqexpr(names, lhs, rhs)]
    elseif isexpr(ex, :comparison)
        return map(2:2:length(ex.args)) do i
            lhs = ex.args[i-1]
            rhs = ex.args[i+1]
            eqexpr(names, lhs, rhs)
        end
    else
        throw(ArgumentError("use `==` in @equation"))
    end
end

function eqexpr(names::Vector{Symbol}, lhs, rhs)
    lt = []
    rt = []
    for term in eachterm(lhs)
        name = filter(x -> inexpr(term, x), names)
        @assert length(name) < 2
        if isempty(name)
            push!(rt, :(-$term))
        else
            push!(lt, termexpr(name[1], term))
        end
    end
    for term in eachterm(rhs)
        name = filter(x -> inexpr(term, x), names)
        @assert length(name) < 2
        if isempty(name)
            push!(rt, term)
        else
            push!(lt, termexpr(name[1], :(-$term)))
        end
    end
    if isempty(rt)
        return :($Equation(0, $(lt...)))
    else
        rt = Expr(:call, :+, rt...)
        return :($Equation($rt, $(lt...)))
    end
end

################################################################################
#
# @linsystem(varset..., expr)
#   construct LinearSystem
#
################################################################################

macro linsystem(args...)
    equations = extract_equations(args[end])
    varset = collect(args[1:end-1])
    esc(systemexpr(equations, varset))
end

function varsetexpr(vars::Vector)
    varset = map(vars) do ex
        if @capture(ex, name_Symbol)
            return ScalarVariable{name}()
        elseif @capture(ex, name_ => dim_)
            return :($(VectorVariable{name})($dim))
        else
            throw(ArgumentError("invalid expression to declare variables"))
        end
    end
    return :($VariableSet($(varset...)))
end

function systemexpr(expr::Vector, vars::Vector)
    names = Symbol[]
    dims = []
    for ex in vars
        @assert (@capture(ex, name_Symbol => dim_) || @capture(ex, name_Symbol)) ||
            throw(ArgumentError("invalid expression to declare variables"))
        push!(names, name)
        push!(dims, dim == nothing ? 1 : dim)
    end

    foreach(expr) do ex
        for (name, dim) in zip(names, dims)
            replace_end!(ex, name, dim)
        end
    end

    eqs = Expr(:tuple, vcat([eqexpr(names, ex) for ex in expr]...)...)
    varset = varsetexpr(vars)
    return :($LinearSystem($eqs, $varset))
end

function replace_end!(expr::Expr, name::Symbol, dim::Int)
    if @capture(expr, $name[index_])
        if index == :end
            expr.args[2] = dim
        elseif isexpr(index, :(:), :vect)
            for i in eachindex(index.args)
                if index.args[i] == :end
                    index.args[i] = dim
                end
            end
        end
    else
        for ex in expr.args
            replace_end!(ex, name, dim)
        end
    end
end
replace_end!(x, name::Symbol, dim) = nothing

################################################################################
#
# @linsolve(varset..., expr)
#   solve linear equations
#
################################################################################

macro linsolve(args...)
    equations = extract_equations(args[end])
    varset = collect(args[1:end-1])
    expr = systemexpr(equations, varset)
    return esc(:($linsolve($expr)))
end

################################################################################
#
# eachterm(expr)
#   split expression into each term
#
################################################################################

function eachterm(ex)
    @capture(ex, (op_Symbol_call)(args__)) ?
        eachterm(SymbolParameter(op), args) : [ex]
end

function eachterm(::SymbolParameter{T}, args::Vector) where {T}
    return [Expr(:call, T, args...)]
end

function eachterm(::SymbolParameter{:+}, args::Vector)
    terms = []
    foreach(args) do ex
        append!(terms, eachterm(ex))
    end
    return terms
end

function eachterm(::SymbolParameter{:-}, args::Vector)
    applyminus = ex -> @capture(ex, -x_) ? x : :(-$ex)
    terms = []
    if length(args) == 1 # -x
        foreach(eachterm(args[1])) do ex
            push!(terms, applyminus(ex))
        end
    else # x - y - z
        append!(terms, eachterm(args[1]))
        foreach(args[2:end]) do ex
            ts = eachterm(ex)
            append!(terms, applyminus.(ts))
        end
    end
    return terms
end

function eachterm(::SymbolParameter{:*}, args::Vector) # this is necessary for 2(x+y)
    terms1 = eachterm(args[1])
    terms2 = eachterm(args[2])
    if length(args) == 2
        if length(terms1) == length(terms2) == 1
            return [Expr(:call, :*, args...)] # return original
        else
            return eachterm(expandterms(terms1, terms2))
        end
    else
        return eachterm(SymbolParameter(:*), [expandterms(terms1, terms2), args[3:end]...])
    end
end

function expandterms(lhs::Vector, rhs::Vector) # expand (a + b) * (c + d)
    terms = []
    for l in lhs, r in rhs
        push!(terms, :($l * $r))
    end
    return Expr(:call, :+, terms...)
end

################################################################################
#
# isvariable(name, ex)
#   check if ex is variable
#
################################################################################

function isvariable(name::Symbol, ex)
    @capture(ex, $name[index_]) || @capture(ex, $name)
end

################################################################################
#
# coefexpr(name, ex)
#   get expression of coefficient
#
################################################################################

function coefexpr(name::Symbol, ex)
    if @capture(ex, (op_Symbol_call)(args__))
        coefexpr(name, SymbolParameter(op), args)
    else
        isvariable(name, ex) ?
            1 : throw(ArgumentError("there is no variable name matching $name"))
    end
end

function coefexpr(name::Symbol, ::SymbolParameter{:+}, args::Vector)
    @assert length(args) == 1
    if isvariable(name, args[1]) # +x
        return 1
    else # +3x
        return coefexpr(name, args[1])
    end
end

function coefexpr(name::Symbol, ::SymbolParameter{:-}, args::Vector)
    @assert length(args) == 1
    if isvariable(name, args[1]) # -x
        return -1
    else # -3x
        return Expr(:call, :-, coefexpr(name, args[1]))
    end
end

function coefexpr(name::Symbol, ::SymbolParameter{:*}, args::Vector)
    newargs = args[1:end-1]
    if !isvariable(name, args[end])
        push!(newargs, coefexpr(name, args[end]))
    end
    if length(newargs) == 1 # 3x
        return newargs[1]
    else
        return Expr(:call, :*, newargs...)
    end
end

################################################################################
#
# indexexpr(name, expr)
#   get expression of index
#
################################################################################

function indexexpr(name::Symbol, expr)
    if @capture(expr, $name[index_]) || @capture(expr, $name)
        return index == nothing ? :(:) : index
    else
        !isa(expr, Expr) &&
            throw(ArgumentError("there is no variable name matching $name"))
        return indexexpr(name, expr.args[end])
    end
end
