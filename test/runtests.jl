using LinearSystems
using Base.Test

include("macros.jl")

const TOL = 1e-10

@testset "Scalar variables" begin
    @testset "@linsolve" begin
        x, y = @linsolve x y begin
             x +  y == 3
            2x + 5y == 9
        end
        @test x ≈ 2
        @test y ≈ 1

        x, y = @inferred linsolve(@linsystem x y begin
             x +  y == 3
            2x + 5y == 9
        end)
        @test x ≈ 2
        @test y ≈ 1

        x, y = @inferred linsolve(@linsystem x y begin
            x + y == sum([0, 1, 2])
            2/5*5x + 5y == 18/2
        end)
        @test x ≈ 2
        @test y ≈ 1

        x, y = @inferred linsolve(@linsystem x y begin
            x + y == 3
            2//5*5x + 5y == 18//2
        end)
        @test x ≈ 2
        @test y ≈ 1
        @test isa(x, Rational{Int})
        @test isa(y, Rational{Int})

        x, y = @inferred linsolve(@linsystem x y begin
            3(x - 2y) + 10 == y - 7
            6x + 5y == 4
        end)
        @test x ≈ -1
        @test y ≈ 2

        sol = @inferred linsolve(@linsystem x y z begin
             x +  y == 3
            2y + 5z == 9
            3x +  z == 1
        end)
        y = 49/17
        x = 3 - y
        z = 1 - 3x
        @test sol[1] ≈ x
        @test sol[2] ≈ y
        @test sol[3] ≈ z

        x, y = @inferred linsolve(@linsystem x y begin
            x +  y == 3
                3y == 9
        end)
        @test x ≈ 0
        @test y ≈ 3

        sol = @inferred linsolve(@linsystem x y z begin
             x +  y == 3
            2y + 5z == 9
                  z == 2
        end)
        z = 2
        y = (9 - 5z) / 2
        x = 3 - y
        @test sol[1] ≈ x
        @test sol[2] ≈ y
        @test sol[3] ≈ z
    end
end

@testset "Vector variables" begin
    @testset "@linsolve" begin
        A = rand(3,3)

        x, y = @inferred linsolve(@linsystem x=>3 y=>3 begin
            A*y == x
            x[2] == x[3]
            x[1] + x[2] + x[3] == 0.0
            y[1] == 0.1
        end)
        @test isapprox(A*y, x, atol=TOL)
        @test isapprox(x[2], x[3], atol=TOL)
        @test isapprox(x[1] + x[2] + x[3], 0.0, atol=TOL)
        @test isapprox(y[1], 0.1, atol=TOL)
    end
    @testset "complicated examples" begin
        A = rand(3,3)

        x, y = @inferred linsolve(@linsystem x=>3 y=>3 begin
            A*y == x
            x[2] == x[end] # check single end
            ones(3)' * x == 0.0 # check non-square matrix and colon index
            y[1] == 0.1
            y[4:end] == 2.0 # check empty index
        end)
        @test isapprox(A*y, x, atol=TOL)
        @test isapprox(x[2], x[3], atol=TOL)
        @test isapprox(x[1] + x[2] + x[3], 0.0, atol=TOL)
        @test isapprox(y[1], 0.1, atol=TOL)

        x, y = @inferred linsolve(@linsystem x=>3 y=>3 begin
            A*y == x
            x[2:end] == 0.0 # check range with end
            y[1] == 0.1
        end)
        @test isapprox(A*y, x, atol=TOL)
        @test isapprox(x[2], 0.0, atol=TOL)
        @test isapprox(x[3], 0.0, atol=TOL)
        @test isapprox(y[1], 0.1, atol=TOL)

        I = [1,2,3]
        x, y = @inferred linsolve(@linsystem x=>3 y=>3 begin
            A*y == x
            x[2] == x[3]
            ones(3)' * x[I[1:end]] == 0.0 # check non-square matrix and vector index with given index
            y[1] == 0.1
        end)
        @test isapprox(A*y, x, atol=TOL)
        @test isapprox(x[2], x[3], atol=TOL)
        @test isapprox(x[1] + x[2] + x[3], 0.0, atol=TOL)
        @test isapprox(y[1], 0.1, atol=TOL)

        x, y = @inferred linsolve(@linsystem x y=>3 begin
            map(x->x,[1,2,3])*x == y
            y[1] + y[2] + y[3] == 1.0
        end)
        @test isapprox([1,2,3]*x, y, atol=TOL)
        @test isapprox(sum(y), 1.0, atol=TOL)

        n = 3
        x, y = @inferred linsolve(@linsystem x=>n y=>n begin
            A*y == x
            x[2] == x[3] == 0.0
            y[1] == 2.0
        end)
        @test isapprox(A*y, x, atol=TOL)
        @test isapprox(x[2], 0.0, atol=TOL)
        @test isapprox(x[3], 0.0, atol=TOL)
        @test isapprox(y[1], 2.0, atol=TOL)
    end
end
