using LinearSystems: eachterm, isvariable, coefexpr, indexexpr, eqexpr

x = y = z = 1
@testset "macros" begin
    @test eval.(eachterm(:(x + 2y - z))) == [1, 2, -1]
    @test eval.(eachterm(:(-x + 2*(2+3)*y - 2/2*3*2z))) == [-1, 4, 6, -6]
    @test eval.(eachterm(:(-x + 2((2+3)*y - 3/2*2*3*z)))) == [-1, 4, 6, -18]

    @test isvariable(:x, :x) == true
    @test isvariable(:x, :(x[1])) == true
    @test isvariable(:x, :(3*x[1])) == false

    @test eval(coefexpr(:x, :x)) == 1
    @test eval(coefexpr(:x, :(-x))) == -1
    @test eval(coefexpr(:x, :(-3x))) == -3
    @test eval(coefexpr(:x, :(+x))) == 1
    @test eval(coefexpr(:x, :(+3x))) == 3
    @test eval(coefexpr(:x, :(2*3*x))) == 6
    @test eval(coefexpr(:x, :(x[1]))) == 1
    @test eval(coefexpr(:x, :(3/2*2x[1]))) == 3
    @test_throws ArgumentError coefexpr(:x, :y)
    @test_throws ArgumentError coefexpr(:x, :(+y))
    @test_throws ArgumentError coefexpr(:x, :(-y))
    @test_throws ArgumentError coefexpr(:x, :(2*3*y))
    @test_throws ArgumentError coefexpr(:x, :(y[1]))
    @test_throws ArgumentError coefexpr(:x, :(3/2*2y[1]))

    @test eval(indexexpr(:x, :(3/2*2x[1]))) == 1
    @test eval(indexexpr(:x, :(3x[:]))) == Colon()
    @test eval(indexexpr(:x, :(x[1:3]))) == 1:3
    @test_throws ArgumentError indexexpr(:x, :(y[1:3]))
    @test_throws ArgumentError indexexpr(:x, :(y))
    @test_throws ArgumentError indexexpr(:x, :(3y))
    @test_throws ArgumentError indexexpr(:x, :(3*y[1:2]))

    @test_throws ArgumentError eqexpr([:x, :y], :(x + y))
end
